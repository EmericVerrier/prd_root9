import math


class Site:
    """
    This class represents a basic site with a site id attribute and an instance linked to it
    """
    def __init__(self, id_site=None, instance=None, x: int = 0, y: int = 0):
        self._id_site: int = id_site
        self._instance = instance
        self._x: int = x
        self._y: int = y

    def read_coordinates(self, line: str):
        value = line.replace('\n', '')
        value = value.split('\t')
        self._x, self._y = int(value[0]), int(value[1])

    def distance_to(self, other_site: 'Site'):
        return math.sqrt((other_site._x - self._x)**2 + (other_site._y - self._y)**2)

    @property
    def id_site(self) -> int:
        return self._id_site

    @property
    def y(self) -> int:
        return self._y

    @property
    def x(self) -> int:
        return self._x

    @x.setter
    def x(self, x: int):
        self._x = x

    @y.setter
    def y(self, y: int):
        self._y = y
