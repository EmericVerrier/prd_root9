import math
import random
from typing import List, Union
from Producer import Producer
from Client import Client
from Hub import Hub
import Site
import TspInstance
import json


class Lrp2eInstance:
    """This class represents a 2e-lrp instance sites contains in this order : the producers, the hubs and the clients.
    This order is important -> PHC"""
    def __init__(self,
                 nb_clients: int = -1,
                 nb_hubs: int = -1,
                 nb_producers: int = -1,
                 sites=None,
                 distances=None,
                 hub_opening_costs=None,
                 producer_opening_costs=None,
                 hub_capacities=None,
                 vf_level_cap: int = -1,
                 vs_level_cap: int = -1,
                 cost_first_level_vehicle: int = -1,
                 cost_second_level_vehicle: int = -1,
                 filename: str = None):
        self._nb_clients: int = nb_clients
        self._nb_hubs: int = nb_hubs
        self._nb_producers: int = nb_producers
        self._sites: List[Union[Producer, Hub, Client]] = sites
        self._distances: List[List[int]] = distances

        self._hub_opening_costs: List[int] = hub_opening_costs
        self._producer_opening_costs: List[int] = producer_opening_costs

        self._hub_capacities: List[int] = hub_capacities
        self._capacity_first_level_vehicle: int = vf_level_cap
        self._capacity_second_level_vehicle: int = vs_level_cap

        self._cost_first_level_vehicle = cost_first_level_vehicle
        self._cost_second_level_vehicle = cost_second_level_vehicle
        self._events_to_process = 0
        self._instance_filename : str = filename

    @classmethod
    def parse_lrp2e_instance_file(cls, path_file_lrp2e_instance: str) -> "Lrp2eInstance":
        """
        Parse a file in the LRP2E instance format and return an instance of the `Lrp2eInstance` class.
        :param cls: The class of the method.
        :param path_file_lrp2e_instance: The path to the file to be parsed.
        :type path_file_lrp2e_instance: str
        :return: An instance of the `Lrp2eInstance` class.
        :rtype: Lrp2eInstance
        """
        with open(path_file_lrp2e_instance, "r") as file_instance:
            cpt = 0
            coords_readen = satellite_caps_readen = demands_readen = opening_costs_readen = False
            instance = Lrp2eInstance(sites=[], filename=path_file_lrp2e_instance.split("/")[-1])
        with open(path_file_lrp2e_instance, "r+") as file_instance:
            content = file_instance.readlines()
            instance._nb_clients = int(content[0])
            instance._nb_hubs = int(content[1])
            instance._nb_producers = int(content[2]) if not content[2].isspace() else 5
            instance._sites.extend([Producer(p, instance=instance)
                                    for p in range(instance._nb_producers)])
            instance._sites.extend([Hub(h, instance=instance)
                                    for h in range(instance._nb_producers, instance._nb_hubs + instance._nb_producers)])
            instance._sites.extend([Client(c, instance=instance)
                                    for c in range(instance._nb_producers + instance._nb_hubs,
                                                   instance._nb_producers + instance._nb_hubs + instance._nb_clients)])
            if content[2].isspace():
                cls.add_producers(path_file_lrp2e_instance, instance, instance._nb_producers)
                file_instance.seek(0, 0)
                content = file_instance.readlines()
            for num_line, line in enumerate(content[3:]):
                if line.isspace() or line.startswith("#"):
                    continue
                elif cpt < instance._nb_producers + instance._nb_hubs + instance._nb_clients and not coords_readen:
                    instance._sites[cpt].read_coordinates(line=line)
                    cpt += 1
                    coords_readen = True if cpt == instance._nb_producers+instance._nb_hubs+instance._nb_clients else False
                    cpt = 0 if coords_readen else cpt
                elif instance._capacity_second_level_vehicle == -1:
                    instance._capacity_second_level_vehicle = int(line)
                elif instance._capacity_first_level_vehicle == -1:
                    instance._capacity_first_level_vehicle = int(line)
                elif not satellite_caps_readen and coords_readen and cpt < instance._nb_hubs:
                    instance._sites[instance._nb_producers+cpt].capacity = int(line)
                    cpt += 1
                    satellite_caps_readen = True if cpt == instance._nb_hubs else False
                    cpt = 0 if cpt == instance._nb_hubs else cpt
                elif not demands_readen and satellite_caps_readen and cpt < instance._nb_clients:
                    splitted_line = line.split("\t")
                    for index, dem in enumerate(splitted_line):
                        instance._sites[index].commands[instance._nb_producers+instance._nb_hubs+cpt] = int(dem)
                    demands = {instance._sites[index_p]: int(val)
                               for index_p, val in enumerate(splitted_line)}
                    instance._sites[cpt+instance._nb_hubs+instance._nb_producers].demands = demands
                    cpt += 1
                    demands_readen = True if cpt == instance._nb_clients else False
                    cpt = 0 if demands_readen else cpt
                elif coords_readen and satellite_caps_readen and demands_readen and cpt < instance._nb_hubs:
                    instance._sites[instance._nb_producers+cpt].opening_cost = int(line)
                    cpt += 1
                    opening_costs_readen = True if cpt == instance._nb_hubs else False
                    cpt = 0 if opening_costs_readen else cpt
                elif instance._cost_first_level_vehicle == -1:
                    instance._cost_first_level_vehicle = int(line)
                elif instance._cost_second_level_vehicle == -1:
                    instance._cost_second_level_vehicle = int(line)
            instance._distances = [[round(math.sqrt((arr.x - dep.x)**2 + (arr.y-dep.y)**2))
                                   for arr in instance._sites]
                                   for dep in instance._sites]
            return instance

    @staticmethod
    def add_producers(path_file_lrp2e_instance: str, instance: "Lrp2eInstance", nb_producer: int):
        nb_readen_blocks = 0
        with open(path_file_lrp2e_instance, "r") as fic:
            content = fic.readlines()
            content.insert(2, str(nb_producer) + '\n')
            index = content.index('\n')
            content.pop(index + 1)
            content.insert(3, "\n# producers positions block\n")
            for index in range(nb_producer):
                content.insert(4, f'{random.randint(0, 50)}\t{random.randint(0, 50)}\n')
            for num_line in range(1, len(content)-1):
                if content[num_line-1].isspace() and not content[num_line].isspace():
                    nb_readen_blocks += 1
                if nb_readen_blocks == 5:
                    for d in range(instance._nb_clients):
                        content[num_line+d] = "\t".join([str(random.randint(11, 20)) for p in range(instance._nb_producers)])+'\n'
                    break
        with open(path_file_lrp2e_instance, "w") as fic:
            fic.writelines(content)

    """
    This function allows you to get one or many tsp instances with a lr2pe instance in entry. An instance of the tsp is
    created for each producer. Clients are included in the instance when there is a strictly positive demand for the 
    producer. Distances are 2d euclidean distances.
    """
    def convert_from_lrp2e_to_tsp(self) -> List[TspInstance.TspInstance]:
        """
        Convert the current Lrp2eInstance object into a list of TspInstance objects.

        :return: A list of TspInstance objects, one for each producer in the current Lrp2eInstance.
        :rtype: List[TspInstance.TspInstance]
        """
        tsp_instances_list = []
        index_s = 0
        for s in self.sites:
            if type(s) is Producer:
                clients = [self.sites[client_id] for client_id in s.commands.keys()]
                sites = [s] + clients
                distances = [[self.distances[tsp_site_dep.id_site][tsp_site_arr.id_site]
                              for tsp_site_arr in sites]
                             for tsp_site_dep in sites]
                tsp_instances_list.append(TspInstance.TspInstance(sites=sites,
                                                                  distances=distances,
                                                                  instance_filename="TSP_" + self._instance_filename.replace('.dat', f'-{index_s}.tsp')))
                index_s += 1
        return tsp_instances_list

    def __str__(self):
        return f"nb_clients : {self.nb_clients} \n nb_hubs : {self.nb_hubs}\n" \
               f"hub_opening_cost : {self.hub_opening_costs} \n producer_opening_cost : {self.producer_opening_costs} "

    def json_export(self):
        output_file = open("instance_export", "x")
        chaine = json.dumps(self.__dict__, output_file)
        return chaine


    @property
    def nb_producers(self) -> int:
        return self._nb_producers

    @property
    def nb_hubs(self) -> int:
        return self._nb_hubs

    @property
    def nb_clients(self) -> int:
        return self._nb_clients

    @property
    def capacity_first_level(self) -> int:
        return self._capacity_first_level_vehicle

    @property
    def capacity_second_level(self) -> int:
        return self._capacity_second_level_vehicle

    @property
    def hub_opening_costs(self) -> List[int]:
        return self._hub_opening_costs

    @property
    def producer_opening_costs(self) -> List[int]:
        return self._producer_opening_costs

    @property
    def distances(self) -> List[List[int]]:
        return self._distances

    @property
    def sites(self) -> list:
        return self._sites

    @property
    def events_to_process(self):
        return self._events_to_process

    def get_site_from_id(self, id_site: int) -> Site.Site:
        return self._sites[id_site]

    @sites.setter
    def sites(self, sites):
        self._sites = sites

    @distances.setter
    def distances(self, distances: List[List[int]]):
        for index, origin_point in enumerate(distances):
            if len(distances[index]) != len(distances):
                raise ValueError("distance's list must have equals height and width")
        self._distances = distances
