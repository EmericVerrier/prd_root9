import math
from Client import Client
from Hub import Hub
import Lrp2eInstance
from Producer import Producer
from typing import Dict, List, Callable, Tuple
from RouteProducer import RouteProducer
from RouteHub import RouteHub
from Site import Site


class Solution:
    """
    Class solution represents a solution of Lrp2eInstance it contains
    _instance : a reference attribute on the corresponding instance,
    _total_degradation : a positive float value for keeping track of solution's value
    _processed_demands : a dictionary where the keys are client site's id and the value is another dict in which the
    key is the producer's site id and the value a tuple composed of the amount of desired product and of a boolean true
    if the demand is processed and false otherwise
    """
    def __init__(self, instance: Lrp2eInstance.Lrp2eInstance):
        """
        Initializes a new instance of the LRP2E solver.
        :param instance: The Lrp2eInstance instance to solve.
        """
        self._instance: Lrp2eInstance.Lrp2eInstance = instance
        self._opening_hubs_status: Dict[Hub, bool] = {s: False for s in self._instance.sites if type(s) is Hub}
        self._load_hubs: Dict[Hub, int] = {s: 0 for s in self._instance.sites if type(s) is Hub}
        self._routes_hubs: Dict[Hub, List[RouteHub]] = {h: [RouteHub(h, self._instance)]
                                                        for h in self._instance.sites if type(h) is Hub}
        self._routes_producers: Dict[Producer, RouteProducer] = {p: RouteProducer(p, self._instance)
                                                                 for p in self._instance.sites if type(p) is Producer}
        self._total_degradation: float = 0.0
        self._processed_demands: Dict[Client, Dict[Producer, List[int, bool]]] = {site: {dem: [site.demands[dem], False]
                                                                                  for dem in site.demands}
                                                                                  for site in self.instance.sites
                                                                                  if type(site) == Client}
        self.processed_events = 0

    def doable(self) -> bool:
        """ Compare instance constraints to routes degradation and return True if all route_degradation are below
        capacity constraints and if all commands or demands are processed """
        for hub in self.routes_hubs:
            for rh in self.routes_hubs[hub]:
                if not rh.doable():
                    return False
        if self.processed_events == self.instance.events_to_process:
            return True
        return False

    def producers_only_solution(self):
        for cli in self._processed_demands:
            for prod in self._processed_demands[cli]:
                self.add_demand(pro=prod, cli=cli, direct_route=True)

    def first_available_hub_route(self, producer: Producer, client: Client, hub: Hub) -> RouteHub:
        """For a given hub return the first available routeHub. It makes the sum of the demand value with the load
        then return the first RouteHub object if this sum is less or equal than the second level capacity
        :param producer: the concerned producer
        :param client: the demanding client
        :param hub: the hub where we iterate through RouteHub objects
        :return: the first available RouteHub object if found.

        """
        if self._routes_hubs.get(hub) is not None:
            for rh in self._routes_hubs[hub]:
                if client.demands[producer.id_site] + rh.load < self.instance.capacity_second_level:
                    return rh

    def build_min_degradation_solution(self):
        """
        0 : initialization of the degradations
        1 : consider every demand
        2 : for each hub, create a new route_hub if lowest_loaded route_hub can't take demand
        3 : update quadruplet dictionnary by computing degradation for the new quadruplet made by new route_hub
        4 : determine the minimum degradation value
        5 : insert into the solution
        for each hub where there is no available route_hub, create a new route_hub to evaluate in the degradations list
        :return:
        """

        degradations: Dict[Tuple, float] = {tuple([p, h, rh, c]): 0.0
                                            for c in self.processed_demands
                                            for p in self._routes_producers
                                            for h in self._routes_hubs
                                            for rh in self._routes_hubs.get(h) if self._routes_hubs.get(h) is not None}
        order_insertion = []
        for deg in degradations:
            degradations[deg] = self.get_degradation(deg)

        for cli in self._processed_demands:
            for pro in self._processed_demands[cli]:
                new_rh = None
                for hub in self._routes_hubs:
                    min_rh = self.get_lowest_load_route_hub(hub)
                    if not min_rh.can_take_demand(pro, cli):
                        new_rh = RouteHub(hub, self.instance)
                        degradations[(pro, hub, new_rh, cli)] = self.get_degradation((pro, hub, new_rh, cli))
                    else:
                        degradations[(pro, hub, min_rh, cli)] = self.get_degradation((pro, hub, min_rh, cli))
                inserted_quad = self._min_quad_doable(degradations)
                if new_rh in inserted_quad:
                    self._routes_hubs[inserted_quad[1]].append(new_rh)
                order_insertion.insert(0, inserted_quad)
                self._total_degradation += degradations[inserted_quad]
                self._processed_demands[cli][pro][1] = True
                if inserted_quad[1] not in self._routes_producers[inserted_quad[0]]:
                    self._routes_producers[inserted_quad[0]].add_site(inserted_quad[1])
                if inserted_quad[3] not in inserted_quad[2]:
                    inserted_quad[2].load += self.processed_demands[cli][pro][0]
                    inserted_quad[2].add_site(inserted_quad[3])
                else:
                    inserted_quad[2].load += self.processed_demands[cli][pro][0]
                for p_h_rh_c in list(degradations.keys()):
                    if p_h_rh_c[0] == inserted_quad[0] and p_h_rh_c[3] == inserted_quad[3]:
                        del (degradations[p_h_rh_c])
                        continue
                    if p_h_rh_c[0] == inserted_quad[0] or p_h_rh_c[1] == inserted_quad[1] or p_h_rh_c[2] == p_h_rh_c[2]:
                        degradations[p_h_rh_c] = self.get_degradation(p_h_rh_c)

        return order_insertion

    def get_degradation(self, p_h_rh_c: Tuple[Producer, Hub, RouteHub, Client]) -> float:
        """
        get degradation for a quadruplet Producer, Hub, RouteHub, Client given as parameter.
        """
        degradation = 0.0
        if self._routes_producers[p_h_rh_c[0]] is None and self._routes_hubs[p_h_rh_c[1]] is None:
            r_p = RouteProducer(p_h_rh_c[0], self._instance)
            r_h = RouteHub(p_h_rh_c[1], self._instance)
            degradation = r_h.cost + r_p.cost
        if self._routes_hubs[p_h_rh_c[1]] is not None:
            degradation += p_h_rh_c[2].evaluate_degradation(p_h_rh_c[3])
        if self._routes_producers[p_h_rh_c[0]] is not None:
            degradation += self._routes_producers[p_h_rh_c[0]].evaluate_degradation(p_h_rh_c[1])
        return degradation

    def _min_quad_doable(self, degradations):
        sorted_degradations = sorted(degradations, key=lambda l: degradations[l])
        filtered = filter(lambda p_h_rh_c: p_h_rh_c[2].can_take_demand(p_h_rh_c[0], p_h_rh_c[3]), sorted_degradations)
        res = next(filtered)
        return res

    def update_rh(self, p_h_c: Tuple[Producer, Hub, Client]):
        min_route = min(self._routes_hubs[p_h_c[1]], key=lambda l: self._routes_hubs[p_h_c[1]][l].load)
        if min_route.load + self.processed_demands[p_h_c[2]][p_h_c[0]] > self.instance.capacity_second_level:
            return RouteHub(p_h_c[1], instance=self.instance)

    def get_lowest_load_route_hub(self,hub: Hub) -> RouteHub:
        return min(self._routes_hubs[hub], key=lambda l: l.load)

    def nearest_available_hub(self, producer: Producer, client: Client) -> Hub:
        """This function returns the nearest available hub between a client and a producer.
        checks in a first time if the hub is available to process the command, then
        looks where the minimum distance between available hub and (producer, client) is.
        :return: the nearest available hub if found"""
        min_hub = None
        min_distance = math.inf
        for h in self._load_hubs:
            if self._load_hubs[h] + client.demands[producer.id_site] <= h.capacity:
                sum_d = self.instance.distances[producer.id_site][h.id_site] + \
                        self.instance.distances[client.id_site][h.id_site]
                if sum_d <= min_distance:
                    min_distance = sum_d
                    min_hub = h
        return min_hub

    def add_demand(self, pro: Producer, cli: Client, direct_route: bool,
                   hub_strategy_affectation: Callable[[Producer, Client], Hub] = None,
                   rh_strategy_affectation: Callable[[Producer, Client, Hub], RouteHub] = None) -> None:
        """
        This method adds the demand to the solution and create the routes objects
        :param pro: the concerned producer, which is the target of the demand
        :param cli: the concerned client, which is the origin of the demand
        :param direct_route: a boolean parameter, if True, the client is placed in a RouteProducer object
        :param hub_strategy_affectation: a function to determine which hub has to manage the demand
        :param rh_strategy_affectation: a function to determine which routeHub has to manage the demand
        """
        self.processed_demands[cli][pro][1] = True

        if pro in self._routes_producers and direct_route:
            self._routes_producers[pro].add_site(intermediary=cli)
        self._total_degradation += self._routes_producers[pro].cost
        if direct_route:
            return

        if not direct_route:
            hub = hub_strategy_affectation(pro, cli)
            if rh_strategy_affectation is not None:
                route_hub = rh_strategy_affectation(pro, cli, hub)
                if route_hub is None:
                    route_hub = RouteHub(hub, self._instance)
                    self._routes_hubs[hub].append(route_hub)
                route_hub.add_site(cli)
                route_hub.load += self.processed_demands[cli][pro][0]
                self._load_hubs[hub] += self.processed_demands[cli][pro][0]

    def remove_demand(self, pro: Producer, cli: Client, hub: Hub= None, rh: RouteHub = None):
        self._processed_demands[cli][pro][1] = False
        if rh is not None:
            rh.processed_commands.remove((pro, cli))
            rh.cost -= self.instance.distances[pro.id_site][cli.id_site]
            rh.route.remove(rh.route.index(cli))
    @property
    def processed_demands(self):
        return self._processed_demands

    @property
    def load_hubs(self):
        return self._load_hubs

    @property
    def routes_producers(self) -> Dict[Producer, RouteProducer]:
        return self._routes_producers

    @property
    def routes_hubs(self) -> Dict[Hub, List[RouteHub]]:
        return self._routes_hubs

    @property
    def total_degradation(self) -> float:
        return self._total_degradation

    @property
    def instance(self):
        return self._instance
