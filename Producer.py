from Site import Site
from typing import Dict


class Producer(Site):
    """
    this class represents a producer. It inherits from the Site's class and
    embeds an orders attribute
    """
    def __init__(self, id_site: int, instance=None, commands=None):
        super().__init__(id_site, instance)
        self._commands = commands if commands is not None else {}

    @property
    def commands(self):
        return self._commands

    @commands.setter
    def commands(self, commands):
        self._commands = commands
