from Producer import Producer
from Site import Site
from Client import Client
from Hub import Hub
import typing
from abc import ABC, abstractmethod
from Lrp2eInstance import Lrp2eInstance


class Route(ABC):
    """
    This class contains a cost attribute which is a float, a list of site, a reference on a solution object
    and a depart point
    """
    def __init__(self,
                 depart_point: typing.Union[Hub, Producer],
                 instance: Lrp2eInstance):

        self._instance: Lrp2eInstance = instance
        self._route: typing.List[Site] = [depart_point, depart_point]
        self._depart_point: typing.Union[Producer, Hub] = depart_point
        self._cost: float = 0.0

        # self.processed_commands: typing.Set[typing.Tuple[Producer, Client]] = set()

    def evaluate_degradation(self, site_evaluated: Site, pos_inser: int = None) -> float:
        """
        Return degradation evaluation if insertion at index pos_inser is None, then, the default value is
        before the route's return point"""
        if site_evaluated in self._route:
            return 0.0
        if pos_inser is None:
            pos_inser = len(self._route)-1
        cost = self._cost
        cost += self._instance.distances[site_evaluated.id_site][self._route[pos_inser-1].id_site]
        cost += self._instance.distances[site_evaluated.id_site][self._route[pos_inser].id_site]
        cost -= self._instance.distances[self._route[pos_inser].id_site][self._route[pos_inser-1].id_site]
        return cost

    @abstractmethod
    def add_site(self, intermediary: typing.Union[Client, Hub], insertion_position: int = None):
        """
        Add a site to the end of the _route attribute, update cost by subtracting distance of the edge depart_point
        and last site visited before insertion of intermediary and adding the distances between depart point
        and intermediary and the distance between the last site visited before insertion to the intermediary.
        :param intermediary:
        :param insertion_position: the position where to insert intermediary site.
        """
        if insertion_position is None:
            insertion_position = len(self._route) - 1
        if intermediary in self.route:
            raise Exception("intermediary already present in the route")
        self._cost += self.evaluate_degradation(site_evaluated=intermediary, pos_inser=insertion_position)
        self._route.insert(insertion_position, intermediary)

    @property
    def route(self):
        return self._route

    @property
    def instance(self):
        return self._instance

    @property
    def cost(self) -> float:
        return self._cost

    @cost.setter
    def cost(self, new_cost: float):
        if new_cost >= 0:
            self._cost = new_cost
        else:
            raise ValueError("Route.cost value must be positive")

    def __contains__(self, item: Site):
        return item in self._route
