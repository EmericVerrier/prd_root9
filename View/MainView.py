import tkinter
from tkinter.filedialog import *


class MainView(tkinter.Tk):
    def __init__(self):
        super(MainView, self).__init__(screenName="PRD")
        self.btn_open_2elrp_instance = tkinter.Button(master=self,
                                                      command=self.open_2e_lrp_instance,
                                                      text="Open 2E-LRP instance")
        self.btn_open_tsp_instance = tkinter.Button(master=self,
                                                    command=self.open_tsp_instance,
                                                    text="Open TSP Instance")

    def open_tsp_instance(self):
        with askopenfile(filetypes=[("tsplib file", "*.tsp")],
                         title="open tsp instance") as tsp_instance_file:
            pass

    def open_2e_lrp_instance(self):
        with askopenfile(filetypes=[("2elrp file", "*.dat")],
                         title="open 2E-LRP instance") as LRP_2e_instance_file:
            pass

