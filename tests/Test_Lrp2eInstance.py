import unittest
import Lrp2eInstance


class TestLrp2eInstance(unittest.TestCase):

    def setUp(self) -> None:
        self.instance = Lrp2eInstance.Lrp2eInstance.parse_lrp2e_instance_file("../data/coord20-5-1-2e.dat")

    def test_nb_clients(self):
        self.assertEqual(self.instance.nb_clients, 20, msg="Client number to read is 20")

    def test_nb_producer(self):
        self.assertEqual(self.instance.nb_producers, 5, msg="Producer number to read is 5")

    def test_nb_hubs(self):
        self.assertEqual(self.instance.nb_hubs, 5, msg="Hub number to read is 5")

    def test_capacity_second_level(self):
        self.assertEqual(self.instance.capacity_second_level, 70)

    def test_capacity_first_level(self):
        self.assertEqual(self.instance.capacity_first_level, 210)

    def test_convert_from_lrp2e_to_tsp(self):
        self.assertEqual(len(self.instance.convert_from_lrp2e_to_tsp()), 5, msg="converted tsp list must be size 5")


if __name__ == '__main__':
    unittest.main()
