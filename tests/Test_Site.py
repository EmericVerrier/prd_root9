import unittest
from Site import Site
import math

class Test_Site(unittest.TestCase):
    def setUp(self) -> None:
        self.site0 = Site(id_site=0, x=0, y=0)
        self.site1 = Site(id_site=1, x=0, y=1)
        self.site2 = Site(id_site=2, x=0, y=2)

    def test_distance_to(self):
        self.assertEqual(self.site0.distance_to(self.site0), 0, "Distances should be 0")
        self.assertEqual(self.site0.distance_to(self.site2), 2, "Distances should be 2")
        self.assertEqual(self.site1.distance_to(self.site0), round(math.sqrt(2)), "Distances shoud be round(square root of 2)")

    def test_read_coordinates(self):
        self.site0.read_coordinates("10\t5")
        self.assertEqual(self.site0.x, 10, "x should be 10 for site 0")
        self.assertEqual(self.site0.y, 5, "y should be 5 for site 0")

if __name__ == '__main__':
    unittest.main()
