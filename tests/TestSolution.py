import unittest
import Solution
import Lrp2eInstance


class TestSolution(unittest.TestCase):

    def test_build_min_degradation_solution(self):
        instance = Lrp2eInstance.Lrp2eInstance.parse_lrp2e_instance_file("../data/coord20-5-1-2e.dat")
        instance2 = Lrp2eInstance.Lrp2eInstance.parse_lrp2e_instance_file("../data/coord50-5-1-2e.dat")
        sol = Solution.Solution(instance)
        sol2 = Solution.Solution(instance)
        self.assertEqual(len(sol.build_min_degradation_solution()), instance.nb_clients*instance.nb_producers)
        self.assertEqual(len(sol2.build_min_degradation_solution()), instance2.nb_clients*instance2.nb_producers)


if __name__ == '__main__':
    unittest.main()
