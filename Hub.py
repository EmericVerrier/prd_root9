from Site import Site


class Hub(Site):
    """
    This class represents a hub site. It contains information on the opening cost,
    a capacity limit and an opening status
    """
    def __init__(self, id_site: int, x: int = 0, y: int = 0, instance=None, capacity: int = 0, cost: int = 0):
        super().__init__(id_site, x=x, y=y, instance=instance)
        self._capacity: int = capacity
        self._rh_level_cap: int = 0
        self._rh_level_cost: int = 0
        self._opening_cost: int = cost

    @property
    def capacity(self) -> int:
        return self._capacity

    @capacity.setter
    def capacity(self, cap: int):
        self._capacity = cap

    @property
    def cost(self) -> int:
        return self._opening_cost

    @property
    def rh_level_cost(self):
        return self._rh_level_cost

    @property
    def rh_level_cap(self):
        return self._rh_level_cap
