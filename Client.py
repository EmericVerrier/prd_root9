from Site import Site
from typing import Dict


class Client(Site):
    """
    This class represents a client site with a demands attribute
    """
    def __init__(self, id_site, x: int = 0, y: int = 0, instance=None, demands= None):
        super().__init__(id_site, x=x, y=y, instance=instance)
        self._demands = demands

    @property
    def demands(self):
        return self._demands

    @demands.setter
    def demands(self, demands: Dict[int, int]):
        self._demands = demands
