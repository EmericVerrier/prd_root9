import Producer
from Route import Route
from Lrp2eInstance import Lrp2eInstance
from Hub import Hub
from Client import Client


class RouteHub(Route):
    def __init__(self, depart_point: Hub, instance: Lrp2eInstance):
        """
        Initialize a new RouteHub object.

        :param depart_point: A Hub object representing the departure point of the RouteHub.
        :type depart_point: Hub
        :param instance: An instance of Lrp2eInstance representing the problem instance.
        :type instance: Lrp2eInstance
        """
        super().__init__(depart_point=depart_point, instance=instance)
        self._load: int = 0
        self._hub_depart: Hub = depart_point
        self._capacity = 0
        self._vehicule_capacity = 0

    def doable(self) -> bool:
        """
        This method returns false if the current load exceed the capacity, else, it returns True
        :return: bool
        """
        return self.load < self.depart_point.capacity

    def can_take_demand(self, pro, cli):
        return cli.demands[pro] + self.load <= self.instance.capacity_second_level
    @property
    def depart_point(self):
        return self._hub_depart

    def add_site(self, intermediary: Client, insertion_position: int = None) -> None:
        """
        Implementation of abstract method add_site, this method check that the inserted site is a client.
        Then it calls the parent part to actually make the insertion
        :param insertion_position
        :param intermediary: Mandatory a Client to be inserted at the end of the route
        :return:
        """
        if type(intermediary) is not Client:
            raise TypeError("sites added in RouteHub objects must be Client")
        super().add_site(intermediary)

    @property
    def load(self):
        return self._load

    @load.setter
    def load(self, load: int):
        if load >= 0:
            self._load = load
        else:
            raise ValueError("current_capacity must be equal or greater than 0")
