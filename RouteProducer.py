from Route import Route
from Producer import Producer
from Client import Client
from Lrp2eInstance import Lrp2eInstance
from Hub import Hub
import typing


class RouteProducer(Route):
    def __init__(self, producer_depart: Producer, instance: Lrp2eInstance):
        super().__init__(depart_point=producer_depart, instance=instance)
        self._producer_depart: Producer = producer_depart

    def depart_point(self):
        return self._producer_depart

    def add_site(self, intermediary: typing.Union[Client, Hub], insertion_position: int = None):
        """
        Implementation of abstract method add_site, this method updates the last hub visited
        Then it calls the parent part to actually make the insertion
        :param intermediary: either a Client or a Hub to be inserted
        :param insertion_position: position where to insert the intermediary
        :type intermediary: Union[Client, Hub]
        :return: None
        """
        if type(intermediary) is Producer:
            raise TypeError("sites added in RouteProducer must be either Client or Hub")
        super().add_site(insertion_position=insertion_position, intermediary=intermediary)
