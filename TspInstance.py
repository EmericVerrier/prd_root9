import Site
from typing import List, Tuple
import os
import subprocess
import re


class TspInstance:
    def __init__(self, sites: [Site.Site] = None,
                 distances: [[int]] = None,
                 instance_filename="",
                 instance_name: str = "",
                 sol_filename: str = None):
        """
        This method initializes a TSP instance with the given parameters.

        :param sites: A list of Site objects representing the nodes in the TSP instance.
        :param distances: A list of lists representing the distances between nodes in the TSP instance.
        :param instance_filename: The filename of the TSP instance file.
        :param instance_name: The name of the TSP instance.
        :param sol_filename: The filename of the solution file for the TSP instance.

        :return: None
        """

        self._sites: List[Site] = sites
        self._distances: List[List[int]] = distances
        self._instance_filename: str = instance_filename
        self._instance_name: str = instance_name
        self._comment: str = ""
        self._sol_filename: str = self._instance_filename.replace(".tsp", ".sol") if sol_filename is None else sol_filename

    def read_sol(self):
        """
        This method reads a solution file and computes the objective function value for a given TSP instance.

        :return: solution_value: A float value representing the objective function value for the TSP instance.
        """
        if not os.getcwd().endswith("data/TSP_INSTANCES"):
            os.chdir("data/TSP_INSTANCES")
        if self._sol_filename in os.listdir(os.getcwd()):
            with open(self._sol_filename, "r") as sol:
                lines = " ".join(sol.readlines()[1:])
                solution_value = 0.0
                split_tour = lines.split()
                solution_value += self._distances[int(split_tour[0])][int(split_tour[-1])]
                for elem in range(len(split_tour)-1):
                    solution_value += self._distances[int(split_tour[elem])][int(split_tour[elem+1])]
            os.chdir("../..")
            return solution_value
        os.chdir("../..")
        return -1

    def dump_to_tsplib_format(self,
                              name: str = None,
                              comments: str = None,
                              edge_matrix: bool = False) -> Tuple:
        """
        :param name: represents the filename of the instance
        :param comments: an optional comment to add to the instance
        :param edge_matrix: if true, distances matrix will be filled in the file else it will be the site's coordinates
        :return: a tuple with the result_string, the name of the instance and the comments

        exports a tsp_instance object into tsplib_format, edges
        will be given under full matrix format in a first time TYPE will always be TSP, NAME and COMMENT will be set as
        given parameters.
        """

        keywords = {"NAME": name if name is not None else f"tsp_instance_{len(self._sites)}.tsp",
                    "COMMENT": comments if comments is not None else "",
                    "TYPE": "TSP",
                    "DIMENSION": f"{len(self.sites)}",
                    "EDGE_WEIGHT_FORMAT": "FULL_MATRIX" if edge_matrix else "FUNCTION",
                    "EDGE_WEIGHT_TYPE": "EUC_2D",
                    "DISPLAY_DATA_TYPE": "COORD_DISPLAY",
                    "EDGE_WEIGHT_SECTION" if edge_matrix else "NODE_COORD_SECTION": "",
                    "NODE_COORD_TYPE": "NO_COORDS" if edge_matrix else "TWOD_COORDS",
                    "EOF": ""
                    }
        name = keywords["NAME"]
        comments = keywords["COMMENT"]
        cont = []
        if edge_matrix:
            cont = [" ".join([str(elem[index_value]) for index_value in range(len(elem))]) for elem in self.distances]
        else:
            cont = [f'{site.id_site} {site.x} {site.y}' for site in self._sites]
        keywords["EDGE_WEIGHT_SECTION" if edge_matrix else "NODE_COORD_SECTION"] = "\n".join(cont)
        result_instance_str = ""
        for k in keywords:
            if k == "EDGE_WEIGHT_SECTION" or k == "NODE_COORD_SECTION":
                result_instance_str += f"{str(k)}\n{keywords[k]}"

            elif k == "EOF":
                result_instance_str += "\n" + str(k)
            else:
                result_instance_str += f"{str(k)}:{str(keywords[k])}\n"
        return result_instance_str, name, comments

    def save_tsp_format_instance_to_file(self,
                                         name: str = None,
                                         comments: str = None,
                                         path_file: str = "./data/TSP_INSTANCES/",
                                         edge_matrix: bool = False):
        """
        This method saves a TSP instance to a file in TSPLIB format.

        :param name: The name of the TSP instance file to be saved.
        :param comments: A string representing comments to be added to the TSP instance file.
        :param path_file: The path where the TSP instance file should be saved.
        :param edge_matrix: A boolean value indicating whether the edge matrix should be used instead of the edge list.

        :return: None
        """
        dumped_instance, name, comments = self.dump_to_tsplib_format(name=name,
                                                                     comments=comments,
                                                                     edge_matrix=False
                                                                     )
        self._instance_filename = name
        if not os.path.exists(path_file):
            os.makedirs(path_file)
        instance_file = open(f'{path_file+name}', "w")
        instance_file.write(dumped_instance)
        instance_file.close()

    def solve_with_concorde(self, concorde_path: str):
        """
        This method uses the Concorde TSP solver to solve the TSP instance.

        :param concorde_path: The path to the Concorde TSP solver executable.

        :return: None
        """
        if not os.getcwd().endswith("data/TSP_INSTANCES"):
            os.chdir("data/TSP_INSTANCES/")
        subprocess.run(args=["../../"+concorde_path, "-x",
                             self._instance_filename])
        self._sol_filename = self._instance_filename.replace(".tsp", ".sol")
        os.chdir("../..")

    @property
    def sites(self) -> List[Site.Site]:
        return self._sites

    @property
    def instance_file_name(self) -> str:
        return self._instance_filename

    @property
    def sol_filename(self) -> str:
        return self._sol_filename

    @property
    def distances(self):
        return self._distances
