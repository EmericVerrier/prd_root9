from Lrp2eInstance import Lrp2eInstance
from Solution import Solution
import View.MainView
import os
if __name__ == "__main__":
    instance = Lrp2eInstance.parse_lrp2e_instance_file("data/coord20-5-1-2e.dat")
    somme_tsp = 0.0
    tsps = instance.convert_from_lrp2e_to_tsp()
    for tsp in tsps:
        somme_tsp += tsp.read_sol()
    sol = Solution(instance)
    sol.build_min_degradation_solution()
    print(f'somme des tsp : {somme_tsp} \t dégradations minimales : {sol.total_degradation}')
    """
    instances_folder = os.listdir("./data")
    for file_name in instances_folder:
        if "dat" in file_name.split("."):
            lrp2e_Instance = Lrp2eInstance.parse_lrp2e_instance_file("./data/"+file_name)
            corresponding_tsp_instances = lrp2e_Instance.convert_from_lrp2e_to_tsp()
            sum_solution = 0.0
            for index_instance, instance in enumerate(corresponding_tsp_instances):
                if instance.instance_file_name not in os.listdir("./data/TSP_INSTANCES"):
                    instance.save_tsp_format_instance_to_file(name=instance.instance_file_name,
                                                              edge_matrix=True)
                if instance.sol_filename not in os.listdir("./data/TSP_INSTANCES"):
                    instance.solve_with_concorde('bin/concorde-bin')
                value_solution = instance.read_sol()
                sum_solution += value_solution
            print(f"somme des TSP : {sum_solution}")
            solution = Solution(lrp2e_Instance)
            solution.build_min_degradation_solution()
            print(f"dégradations minimales : {solution.total_degradation}\n\n")
    print("Terminé")
    input()
"""